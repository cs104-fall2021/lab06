def factorial(n):
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


def permutation(n, k):
    return factorial(n) / factorial(n - k)


print(permutation(6, 4))
