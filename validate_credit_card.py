# Part 1
# 12 -> f -> [1, 2]
# print(give_me_digits(12))
# 33409 -> f -> [3, 3, 4, 0, 9
# print(give_me_digits(33409))
def give_me_digits(number):
    my_digits = []
    for character in str(number):
        my_digits.append(int(character))
    return my_digits


def double_every_other(input_list):
    for i in range(2, len(input_list) + 1, 2):
        input_list[-1 * i] = input_list[-1 * i] * 2
    return input_list


# Part 2
# Example: doubleEveryOther [8,7,6,5] == [16,7,12,5]
# print(double_every_other([8, 7, 6, 5]))
# Example: doubleEveryOther [1,2,3] == [1,4,3]
# print(double_every_other([1, 2, 3]))


def sum_digits(my_list):
    all_digit_list = []
    for number in my_list:
        all_digit_list.extend(give_me_digits(number))

    sum_value = 0
    for digit in all_digit_list:
        sum_value += digit
    return sum_value


# Example: sumDigits [16,7,12,5] = 1 + 6 + 7 + 1 + 2 + 5 = 22
# print(sum_digits([16, 7, 12, 5]))


def validate(credit_card_number):
    digits = give_me_digits(credit_card_number)
    doubled = double_every_other(digits)
    summed = sum_digits(doubled)
    return summed % 10 == 0


# Example: validate 4012888888881881 = True
print(validate(4012888888881881))
# Example: validate 4012888888881882 = False
print(validate(4012888888881882))
